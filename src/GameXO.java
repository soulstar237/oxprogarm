import java.util.Arrays;
import java.util.Scanner;

public class GameXO {
	static String[][] xo;
	static String turn ="X";
	static int R, C,count=0;
	static Scanner kb = new Scanner(System.in);
	static String winner = null;
	
	public static void main(String[] args) {
		printWelcome();
		printBoardempty();
		while (winner == null) {
			Input();
			checkWin();
			checkTurn();
			printBoard();
			count++;
		}
		printWinner();
		}
	
	
	static void printBoardempty() {
		xo = new String[3][3];
		System.out.println();
		for (int i = 0; i < xo.length; i++) {
			for (int j = 0; j < xo.length; j++) {
				xo[i][j] = "-";
			}
		}
		System.out.println("   " + "1	  " + "2  	  " + "3     	");
		System.out.println();
		for (int i = 0; i < 3; i++) {
			System.out.print(i + 1 + "  ");
			for (int j = 0; j < 3; j++) {
				System.out.print(xo[i][j] + "  	  ");
			}
			System.out.println();
			System.out.println();
		}
	}
	static void printWelcome() {
		System.out.println("Welome to XO game");
	}
	static void printBoard() {
		System.out.println("   " + "1	  " + "2  	  " + "3     	");
		System.out.println();
		for (int i = 0; i < xo.length; i++) {
			System.out.print(i + 1 + "  ");
			for (int j = 0; j < xo.length; j++) {
				System.out.print(xo[i][j] + "  	  ");
			}
			System.out.println();
			System.out.println();
		}
	}

	static String Input() {
		if (turn.equals("X")) {
			System.out.print("X (R,C):");
			R = kb.nextInt();
			C = kb.nextInt();
			if (xo[R - 1][C - 1].equals("-"))
				xo[R - 1][C - 1] = "X";
			else
				turn="O";
		} else if (turn.equals("O")) {
			System.out.print("O (R,C):");
			R = kb.nextInt();
			C = kb.nextInt();
			if (xo[R - 1][C - 1].equals("-"))
				xo[R - 1][C - 1] = "O";
			else 
				turn="X";
		}
		return turn;
	}

	static String checkWin() {
		for (int a = 0; a < 8; a++) {
			String line = null;
			switch (a) {
			case 0:
				line = xo[0][0] + xo[0][1] + xo[0][2];
				break;
			case 1:
				line = xo[1][0] + xo[1][1] + xo[1][2];
				break;
			case 2:
				line = xo[2][0] + xo[2][1] + xo[2][2];
				break;
			case 3:
				line = xo[0][0] + xo[1][0] + xo[2][0];
				break;
			case 4:
				line = xo[1][1] + xo[1][2] + xo[0][1];
				break;
			case 5:
				line = xo[2][0] + xo[2][1] + xo[2][2];
				break;
			case 6:
				line = xo[0][0] + xo[1][1] + xo[2][2];
				break;
			case 7:
				line = xo[2][0] + xo[1][1] + xo[0][2];
				break;
			}
			if (line.equals("XXX")) {
				return winner = "X";
			} else if (line.equals("OOO")) {
				return winner = "O";
			}
		}
		return null;
	}
	static String checkTurn() {
		if (turn.equals("X")) {
			turn = "O";
		} else {
			turn = "X";
		}
		return turn;
	}
	static void printWinner() {
		if(count==10) {
			winner="draw";
		}
		if (winner.equals("draw")) {
			System.out.println("draw");
		} else {
			System.out.println(winner + " winner");
		}
	
	}

}